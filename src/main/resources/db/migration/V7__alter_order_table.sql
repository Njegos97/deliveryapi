ALTER TABLE orders
ADD COLUMN users_id int,
ADD COLUMN sent_to VARCHAR(50) NOT NULL DEFAULT '',
ADD FOREIGN KEY (users_id) REFERENCES users(id);
