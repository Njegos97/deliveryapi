CREATE TABLE order_food (
  order_id int NOT NULL,
  food_id int NOT NULL,
  food_quantity integer NOT NULL,
  PRIMARY KEY (order_id, food_id),
  FOREIGN KEY (order_id) REFERENCES orders(id) ON DELETE CASCADE,
  FOREIGN KEY (food_id) REFERENCES food(id) ON DELETE CASCADE
);