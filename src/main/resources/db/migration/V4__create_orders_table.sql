CREATE TABLE orders(
	id  SERIAL PRIMARY KEY,
	date_of_order date NOT NULL,
	price_of_order DECIMAL NOT NULL
);