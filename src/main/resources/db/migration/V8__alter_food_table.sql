ALTER TABLE food
ADD COLUMN food_image_file_url text NOT NULL DEFAULT '',
ADD COLUMN food_image_version int NOT NULL DEFAULT 0;