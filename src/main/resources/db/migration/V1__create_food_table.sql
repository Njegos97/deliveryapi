 CREATE TABLE food(
	id  SERIAL PRIMARY KEY,
	name VARCHAR (50) NOT NULL,
	price DECIMAL NOT NULL,
	type VARCHAR(20) DEFAULT '' NOT NULL,
	is_out_of_stock boolean
);