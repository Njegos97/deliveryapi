package com.bogos.delivery.service.impl;


import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bogos.delivery.entity.Food;
import com.bogos.delivery.enumerator.Type;
import com.bogos.delivery.repository.FoodRepository;
import com.bogos.delivery.service.FoodService;
import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FoodServiceImpl implements FoodService {

	private final FoodRepository foodRepository;
	
	@Value("${CLOUDINARY_URL}")
	String cloudinary_url;
	
	int imageVersion = 0;

	@Override
	public Food save(Food food) {
		
		food.setFoodImageVersion(imageVersion);
		return foodRepository.save(food);
	}

	@Override
	public List<Food> findAll() {
		
		return foodRepository.findAll();
	}

	@Override
	public Food findById(int foodId) {
		
		return foodRepository.findById(foodId).map(food -> {
				return food;
			}).orElseThrow(() -> new RuntimeException("Food with id: " + foodId + " does not exist"));
		
	}

	@Override
	public void deleteById(int foodId) {
		
		 foodRepository.deleteById(foodId);
	}

	@Override
	public Food editFood(Food food) {
		
		food.setFoodImageVersion(imageVersion);
		return foodRepository.save(food);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> uploadFoodImage(String imageUrl, String foodName) {
		
		Map<String, Object> uploadResult = new LinkedHashMap<String, Object>();
		try {
			Cloudinary cloudinary = new Cloudinary(cloudinary_url);			
			uploadResult = cloudinary.uploader().upload(imageUrl, ObjectUtils.asMap
					("public_id", foodName, "invalidate", true, "transformation" ,new Transformation()));
			int version = (int) uploadResult.get("version");
			imageVersion = version;
			return uploadResult;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return uploadResult;
		
	}
	
	@Override
	public String updateImagePublicId(String newPublicId, String oldPublicId) {
		
		Cloudinary cloudinary = new Cloudinary(cloudinary_url);
		try {
			cloudinary.uploader().rename(oldPublicId, newPublicId, ObjectUtils.asMap
					("invalidate", true, "transformation" ,new Transformation()));
			cloudinary.uploader().destroy(oldPublicId, ObjectUtils.asMap());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return newPublicId;
	}

	@Override
	public List<Food> getFoodByType(Type type) {
		
		return foodRepository.findByType(type);
	}
	
}
