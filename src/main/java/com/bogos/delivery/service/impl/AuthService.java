package com.bogos.delivery.service.impl;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import com.bogos.delivery.entity.User;
import com.bogos.delivery.enumerator.Role;
import com.bogos.delivery.service.UserService;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Service
@RequiredArgsConstructor
@Log4j2
public class AuthService extends OncePerRequestFilter {

	private HttpTransport transport;
	private JsonFactory jsonFactory;
	private GoogleIdToken googleIdToken;
	@Value("${google.client.id}")
	String googleClientId;
	private Payload payload;
	private final UserService userService;

	public GoogleIdToken verifyGoogleIdToken(String idToken, HttpServletRequest request)
			throws GeneralSecurityException, IOException {
		this.transport = new NetHttpTransport();
		this.jsonFactory = new JacksonFactory();

		GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
				.setAudience(Collections.singletonList(googleClientId)).build();

		googleIdToken = verifier.verify(idToken);

		return googleIdToken;
	}

	public void logInUser(HttpServletRequest request) {

		if (googleIdToken != null) {
			payload = googleIdToken.getPayload();
			userService.findByEmail(payload.getEmail()).ifPresentOrElse(logedInUser -> logedInUser.getEmail(),
					() -> saveNewUser());

			setAuthentication(request);
			log.info("Token is valid");

		} else {
			log.error("Token is not valid ");
		}

	}

	public User saveNewUser() {
		User user = new User();
		user.setFirstName((String) payload.get("given_name"));
		user.setLastName((String) payload.get("family_name"));
		user.setEmail(payload.getEmail());
		user.setActive(true);
		user.setRole(Role.USER);
		user.setAddress("");
		return userService.saveUser(user);

	}

	public void setAuthentication(HttpServletRequest request) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		authorities.add(new SimpleGrantedAuthority(isAdmin() ? "ROLE_ADMIN" : "ROLE_USER"));

		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(

				payload.getEmail(), payload.getSubject(), authorities);
		authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
		SecurityContextHolder.getContext().setAuthentication(authentication);

	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		String token = getTokenFromHeader(request);
		try {
			GoogleIdToken idToken = verifyGoogleIdToken(token, request);
			if (idToken != null) {
				payload = idToken.getPayload();
				logInUser(request);
				log.info("Token is valid");
			}
		} catch (GeneralSecurityException | IOException | IllegalArgumentException e) {
			log.error("Token is not valid ");
		}
		filterChain.doFilter(request, response);
	}

	private String getTokenFromHeader(HttpServletRequest request) {
		String header = request.getHeader("Authorization");
		String token = "";
		if (header != null && header.startsWith("Bearer ")) {
			token = header.substring(7);
		}
		return token;
	}

	public Boolean isAdmin() {
		Optional<User> user = userService.findByEmail(payload.getEmail());
		if (user.get().getRole() == Role.USER) {
			return false;
		}

		return true;
	}

}
