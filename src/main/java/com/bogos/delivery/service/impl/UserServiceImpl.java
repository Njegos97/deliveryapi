package com.bogos.delivery.service.impl;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.bogos.delivery.entity.User;
import com.bogos.delivery.repository.UserRepository;
import com.bogos.delivery.service.UserService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;
	
	@Override
	public Page<User> findUsersByPage(int pageNumber) {
		Pageable pageable = PageRequest.of(pageNumber, 9);
		Page<User> usersByPage = userRepository.findAll(pageable);
		
		return usersByPage;
	}

	@Override
	public String getIdToken(String idToken) {
		
		return idToken;
	}

	@Override
	public User saveUser(User user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

	@Override
	public Optional<User> findByEmail(String email) {
		// TODO Auto-generated method stub
		return userRepository.findByEmail(email);
	}

	@Override
	public Optional<User> findById(int userId) {
		// TODO Auto-generated method stub
		return userRepository.findById(userId);
	}

	@Override
	public Optional<User> changeRole(User user) {
		// TODO Auto-generated method stub
		Optional<User> editedUser = Optional.of(user);
		userRepository.save(user);
		return editedUser;
	}

}
