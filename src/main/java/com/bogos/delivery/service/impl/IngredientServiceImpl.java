package com.bogos.delivery.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.bogos.delivery.entity.Ingredient;
import com.bogos.delivery.enumerator.Category;
import com.bogos.delivery.enumerator.Type;
import com.bogos.delivery.repository.IngredientRepository;
import com.bogos.delivery.service.IngredientService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class IngredientServiceImpl implements IngredientService {

	private final IngredientRepository ingredientRepository;

	@Override
	public List<Ingredient> findByType(Type type) {
		
		return ingredientRepository.findByType(type);
	}

	@Override
	public Ingredient save(Ingredient ingredient) {
		
		return ingredientRepository.save(ingredient);
	}

	@Override
	public Ingredient editIngredient(Ingredient ingredient) {
		
		return ingredientRepository.save(ingredient);
	}

	@Override
	public List<Ingredient> findAll() {
		
		return ingredientRepository.findAll();
	}

	@Override
	public Optional<Ingredient> findById(int ingredientId) {
		// TODO Auto-generated method stub
		return ingredientRepository.findById(ingredientId);
	}

	@Override
	public void deleteById(int ingredientId) {
		ingredientRepository.deleteById(ingredientId);
		
	}

	@Override
	public Page<Ingredient> findIngredientsByPage(int pageNumber) {
		Pageable pageable = PageRequest.of(pageNumber, 15);
		Page<Ingredient> ingredientsByPage = ingredientRepository.findAll(pageable);
		
		return ingredientsByPage;
	}

	@Override
	public Page<Ingredient> getIngredientByCategoryAndTypeAndPage(Category category, Type type, int pageNumber) {
		Pageable pageable = PageRequest.of(pageNumber, 15);
		Page<Ingredient> ingredientsByPageAndCategory = ingredientRepository.findByCategoryAndType(pageable, category, type);
		
		return ingredientsByPageAndCategory;
	}
}
