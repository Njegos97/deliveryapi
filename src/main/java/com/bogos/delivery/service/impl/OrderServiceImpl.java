package com.bogos.delivery.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.bogos.delivery.entity.Cart;
import com.bogos.delivery.entity.Order;
import com.bogos.delivery.entity.OrderFood;
import com.bogos.delivery.entity.OrderFoodKey;
import com.bogos.delivery.entity.User;
import com.bogos.delivery.enumerator.Role;
import com.bogos.delivery.repository.OrderRepository;
import com.bogos.delivery.repository.UserRepository;
import com.bogos.delivery.service.OrderService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
	
	private final OrderRepository orderRepository;
	private final UserRepository userRepository;

	@Override
	public List<Order> findAll() {

		return orderRepository.findAll();
	}

	@Override
	public Order saveOrder(Set<Cart> foodList, double orderPrice, String sentTo) {

		Order order = new Order();
		order.setDateOfOrder(LocalDate.now());
		order.setPriceOfOrder(orderPrice);
		order.setSentTo(sentTo);
		orderRepository.save(order);
		saveUserThatOrderedFood(order);
		setOrderFood(foodList, order);

		return orderRepository.save(order);
	}

	@Override
	public Optional<Order> findOrder(int orderId) {

		return orderRepository.findById(orderId);
	}

	private User findAuthenticatedUserByEmailIfExists() {
		String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = userRepository.findByEmail(email).orElse(null);
		return user;
	}

	private User saveUserThatOrderedFood(Order order) {
		User user = findAuthenticatedUserByEmailIfExists();
		order.setUser(user);
		return user;
	}

	private void setOrderFood(Set<Cart> foodList, Order order) {

		foodList.forEach(food -> {
			OrderFood orderFood = new OrderFood();
			orderFood.setFood(food.getFood());
			orderFood.setOrder(order);
			orderFood.setFoodQuantity(food.getFoodQuantity());
			OrderFoodKey orderFoodKey = new OrderFoodKey(order.getId(), food.getFood().getId());
			orderFood.setId(orderFoodKey);
			order.getOrderFood().add(orderFood);
		});

	}

	@Override
	public List<Order> getOrderHistory() {

		User user = findAuthenticatedUserByEmailIfExists();
		if(user.getRole() == Role.ADMIN) {
			return findAll();
		}
		
		return orderRepository.findAllByUserId(user.getId());
	}

}
