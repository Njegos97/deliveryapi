package com.bogos.delivery.service;

import java.util.List;

import com.bogos.delivery.entity.OrderFood;

public interface OrderFoodService {
	List<OrderFood> save(List<OrderFood> orderFood);
}
