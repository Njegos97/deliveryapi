package com.bogos.delivery.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import com.bogos.delivery.entity.User;

public interface UserService {

	Page<User> findUsersByPage(int pageNumber);
	
	String getIdToken(String idToken);

	User saveUser(User user);

	Optional<User> findByEmail(String email);

	Optional<User> findById(int userId);

	Optional<User> changeRole(User user);
 }
