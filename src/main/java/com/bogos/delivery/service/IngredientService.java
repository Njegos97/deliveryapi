package com.bogos.delivery.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.bogos.delivery.entity.Ingredient;
import com.bogos.delivery.enumerator.Category;
import com.bogos.delivery.enumerator.Type;

public interface IngredientService {

	List<Ingredient> findByType(Type type);
	
	Ingredient save (Ingredient ingredient);

	Ingredient editIngredient(Ingredient ingredient);

	List<Ingredient> findAll();

	Optional<Ingredient> findById(int ingredientId);

	void deleteById(int ingredientId);

	Page<Ingredient> findIngredientsByPage(int pageNumber);

	Page<Ingredient> getIngredientByCategoryAndTypeAndPage(Category category, Type type, int pageNumber);
}
