package com.bogos.delivery.service;

import java.util.List;
import java.util.Map;

import com.bogos.delivery.entity.Food;
import com.bogos.delivery.enumerator.Type;

public interface FoodService {

	Food save(Food food);

	List<Food> findAll();

	Food findById(int foodId);

	void deleteById(int foodId);

	Food editFood(Food food);

	Map<String, Object> uploadFoodImage(String imageUrl, String foodName);
	
	String updateImagePublicId(String newPublicId, String oldPublicId);

	List<Food> getFoodByType(Type type);

	
}
