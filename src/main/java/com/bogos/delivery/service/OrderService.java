package com.bogos.delivery.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.bogos.delivery.entity.Cart;
import com.bogos.delivery.entity.Order;

public interface OrderService {

	List<Order> findAll();

	Order saveOrder(Set<Cart> foodList, double orderPrice, String sentTo);
	
	Optional<Order> findOrder(int orderId);

	List<Order> getOrderHistory();

}
