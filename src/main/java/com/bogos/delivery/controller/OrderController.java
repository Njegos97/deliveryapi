package com.bogos.delivery.controller;

import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bogos.delivery.entity.Cart;
import com.bogos.delivery.entity.Order;
import com.bogos.delivery.entity.OrderFood;
import com.bogos.delivery.service.OrderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RequestMapping("/api/cart")
@RestController
@RequiredArgsConstructor
@Api(value = "deliveryfood", description = "Operations pertaining to order in Delivery Api")
public class OrderController {

	private final OrderService orderService;
	
	@ApiOperation(value = "Add order", response = ResponseEntity.class)
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Set<Cart>> saveOrder(@RequestBody Set<Cart> foodList, @RequestParam double orderPrice,
			@RequestParam String sentTo) {
		
		orderService.saveOrder(foodList, orderPrice, sentTo);
		return new ResponseEntity<Set<Cart>>(foodList, HttpStatus.CREATED); 
	}

	@ApiOperation(value = "Search ordered food with an ID",response = ResponseEntity.class)
	@GetMapping("/{orderId}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Set<OrderFood>> getOrderedFoodById(@PathVariable int orderId) {
		
		Order order = orderService.findOrder(orderId).get();
		Set<OrderFood> orderFood = order.getOrderFood();
	
		return new ResponseEntity<Set<OrderFood>>(orderFood, HttpStatus.OK);
	}
	
	@ApiOperation(value = "View a list of available order history", response = ResponseEntity.class)
	@GetMapping()
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	public ResponseEntity<List<Order>> getUserOrderHistory() {
		
		List<Order> orderList = orderService.getOrderHistory();
		return new ResponseEntity<List<Order>>(orderList, HttpStatus.OK);
	}
	
}
