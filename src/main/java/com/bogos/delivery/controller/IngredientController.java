package com.bogos.delivery.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bogos.delivery.entity.Ingredient;
import com.bogos.delivery.enumerator.Category;
import com.bogos.delivery.enumerator.Type;
import com.bogos.delivery.service.IngredientService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RequestMapping("/api/ingredient")
@RestController
@RequiredArgsConstructor
@Api(value = "deliveryfood", description = "Operations pertaining to ingredients in Delivery Api")
public class IngredientController {
	
	private final IngredientService ingredientService;
	
	@ApiOperation(value = "View a list of available ingredients by type", response = ResponseEntity.class)
	@GetMapping("/type")
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Ingredient>> getIngredientByType(@RequestParam(required = true) Type type){
		
		List<Ingredient> ingredientsByType = ingredientService.findByType(type);
		return new ResponseEntity<List<Ingredient>>(ingredientsByType, HttpStatus.OK);
	}
	
	@ApiOperation(value = "View a list of available ingredients", response = ResponseEntity.class)
	@GetMapping()
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Ingredient>> getAllIngredients(){
		
		List<Ingredient> allIngredients = ingredientService.findAll();
		return new ResponseEntity<List<Ingredient>>(allIngredients, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Add a ingredient", response = ResponseEntity.class)
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Ingredient> saveIngredient(@Valid @RequestBody Ingredient ingredient){
		
		ingredientService.save(ingredient);
		return new ResponseEntity<Ingredient>(ingredient, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Update a ingredient")
	@PutMapping("/{ingredientId}")
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Ingredient> editIngredient(@Valid @RequestBody Ingredient ingredient,
			@PathVariable int ingredientId){
		
		ingredient.setId(ingredientId);
		Ingredient editedIngredient = ingredientService.editIngredient(ingredient);
		return new ResponseEntity<Ingredient>(editedIngredient, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Search a ingredient with an ID",response = ResponseEntity.class)
	@GetMapping("/{ingredientId}")
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Ingredient> getIngredient(@PathVariable int ingredientId){
		
		return ingredientService.findById(ingredientId).map(ResponseEntity::ok).orElseGet(ResponseEntity.notFound()::build);
	}
	
	@ApiOperation(value = "Delete a ingredient")
	@DeleteMapping("/{ingredientId}")
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void deleteById(@PathVariable int ingredientId) {
		
		ingredientService.deleteById(ingredientId);
	}
	
	@GetMapping("/page")
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Page<Ingredient>> getIngredientsByPage(@RequestParam int pageNumber) {
		
		Page<Ingredient> ingredientsByPage = ingredientService.findIngredientsByPage(pageNumber);
		return new ResponseEntity<Page<Ingredient>>(ingredientsByPage, HttpStatus.OK);
	}
	
	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/category")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Page<Ingredient>> getIngredientByCategoryAndType(@RequestParam int pageNumber, @RequestParam(required = false) Category category, @RequestParam(required = false) Type type){
		
		Page<Ingredient> ingredientByCategory = ingredientService.getIngredientByCategoryAndTypeAndPage(category, type, pageNumber);
		return new ResponseEntity<Page<Ingredient>>(ingredientByCategory, HttpStatus.OK);
	}

}
