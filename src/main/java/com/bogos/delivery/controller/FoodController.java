package com.bogos.delivery.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bogos.delivery.entity.Food;
import com.bogos.delivery.enumerator.Type;
import com.bogos.delivery.service.FoodService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RequestMapping("/api/food")
@RestController
@RequiredArgsConstructor
@Api(value = "deliveryfood", description = "Operations pertaining to food in Delivery Api")
public class FoodController {

	private final FoodService foodService;

	@PostMapping("/image")
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Map<String, Object>> saveImage(@RequestBody String imageUrl, @RequestParam String foodName) {
		
		Map<String, Object> uploadResult = new LinkedHashMap<String, Object>();
		uploadResult = foodService.uploadFoodImage(imageUrl, foodName);
		return new ResponseEntity<Map<String, Object>>(uploadResult, HttpStatus.CREATED);

	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "View a list of available food", response = ResponseEntity.class)
	public ResponseEntity<List<Food>> getAll() {

		List<Food> allFood = foodService.findAll();
		return new ResponseEntity<List<Food>>(allFood, HttpStatus.OK);
	}

	@ApiOperation(value = "Add a food")
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Food> save(@Valid @RequestBody Food food) {

		foodService.save(food);
		return new ResponseEntity<Food>(food, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Search a food with an ID",response = ResponseEntity.class)
	@GetMapping("/{foodId}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Food> getById(@PathVariable int foodId) {

		Food foodWithGivenId = foodService.findById(foodId);
		return new ResponseEntity<Food>(foodWithGivenId, HttpStatus.OK);
	}

	@ApiOperation(value = "Delete a food")
	@DeleteMapping("/{foodId}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@ResponseStatus(HttpStatus.OK)
	public void deleteById(@PathVariable int foodId) {
		
		foodService.deleteById(foodId);
	}

	@ApiOperation(value = "Update a food")
	@PutMapping("/{foodId}")
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Food> editFood(@Valid @RequestBody Food food, @PathVariable int foodId,
			@RequestParam boolean isImageChanged) {

		food.setId(foodId);
		Food oldFood = foodService.findById(foodId);
		if (!food.getName().equals(oldFood.getName()) && !isImageChanged) {
			foodService.updateImagePublicId(food.getName(), oldFood.getName());
		}
		Food editedFood = foodService.editFood(food);
		return new ResponseEntity<Food>(editedFood, HttpStatus.OK);
	}
	
	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/type")
	public ResponseEntity<List<Food>> getFoodByType(@RequestParam Type type){
		
		List<Food> foodByType = foodService.getFoodByType(type);
		return new ResponseEntity<List<Food>>(foodByType, HttpStatus.OK);
	}

}
