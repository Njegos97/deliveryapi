package com.bogos.delivery.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bogos.delivery.entity.User;
import com.bogos.delivery.service.UserService;
import com.bogos.delivery.service.impl.AuthService;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RequestMapping("/api/user")
@RestController
@RequiredArgsConstructor
@Api(value = "deliveryfood", description = "Operations pertaining to user in Delivery Api")
public class UserController {
	
	private final UserService userService;
	private final AuthService authService;
	
	@ApiOperation(value = "View a list of available users by page", response = ResponseEntity.class)
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Page<User>> getUsersByPage(@RequestParam int pageNumber) {
		
		Page<User> usersByPage = userService.findUsersByPage(pageNumber);
		return new ResponseEntity<Page<User>>(usersByPage, HttpStatus.OK);
	}
	
	@PostMapping("/id-token")
	public GoogleIdToken googleIdToken(@RequestBody String idToken, HttpServletRequest req) throws GeneralSecurityException, IOException{
	
		return authService.verifyGoogleIdToken(idToken, req);
	}
	

	@ApiOperation(value = "Return logged in user",response = ResponseEntity.class)
	@GetMapping("/logged-in")
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	public ResponseEntity<User> getLoggedInUser(Authentication auth) {

		return userService.findByEmail(auth.getName()).map(ResponseEntity::ok)
				.orElseGet(ResponseEntity.notFound()::build);
	}
	
	@ApiOperation(value = "Search a user with an ID",response = ResponseEntity.class)
	@GetMapping("/{userId}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<User> getById(@PathVariable int userId){
		
		return userService.findById(userId).map(ResponseEntity::ok).orElseGet(ResponseEntity.notFound()::build);
	}
	
	@ApiOperation(value = "Change role of a user")
	@PutMapping("/role-change")
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<User> changeRole(@RequestBody User user){
		return userService.changeRole(user).map(ResponseEntity::ok).orElseGet(ResponseEntity.notFound()::build);
	}

}
