package com.bogos.delivery.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bogos.delivery.entity.Ingredient;
import com.bogos.delivery.enumerator.Category;
import com.bogos.delivery.enumerator.Type;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Integer> {

	List<Ingredient> findByType(Type type);

	@Query("SELECT i FROM Ingredient i WHERE (:category is null or i.category = :category) and (:type is null"
			  + " or i.type = :type)")
	Page<Ingredient> findByCategoryAndType(Pageable pageable, @Param("category") Category category, @Param("type") Type type);

}
