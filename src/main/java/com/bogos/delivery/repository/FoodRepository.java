package com.bogos.delivery.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bogos.delivery.entity.Food;
import com.bogos.delivery.enumerator.Type;

@Repository
public interface FoodRepository extends JpaRepository<Food, Integer> {

	Optional<Food> findById(int foodId);

	List<Food> findByType(Type type);
}
