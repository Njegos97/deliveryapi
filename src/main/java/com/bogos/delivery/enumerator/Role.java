package com.bogos.delivery.enumerator;

public enum Role {

	USER,
	ADMIN
}
