package com.bogos.delivery.enumerator;

public enum Type {

	PIZZA, 
	BURGER, 
	TACOS,
	SANDWICH
}
