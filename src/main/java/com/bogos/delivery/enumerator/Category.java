package com.bogos.delivery.enumerator;

public enum Category {

	CHEESE,
	MEAT,
	SALAD,
	SAUCE
	
}
