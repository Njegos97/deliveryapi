package com.bogos.delivery.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.bogos.delivery.enumerator.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotBlank
	@Size(max = 45)
	private String firstName;

	@NotBlank
	@Size(max = 45)
	private String lastName;

	@NotBlank
	@Size(max = 45)
	private String email;

	@Enumerated(EnumType.STRING)
	private Role role;

	private boolean active;

	private String address;
	

}
