package com.bogos.delivery.entity;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.bogos.delivery.enumerator.Type;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Food {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotBlank
	@Size(max = 50)
	@Size(min = 2)
	private String name;
	
	@NotNull
	private double price;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private Type type;
	
	@NotNull
	private boolean isOutOfStock;
	
	@Size(max = 200)
	private String description; 
	
	private int foodImageVersion;
	
	 @OneToMany(mappedBy = "food", cascade = CascadeType.ALL)
	 @JsonIgnore
	 private Set<OrderFood> orderFood = new HashSet<>();
	 
	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Food other = (Food) obj;
		return Objects.equals(id, other.getId());
	}
		
}
