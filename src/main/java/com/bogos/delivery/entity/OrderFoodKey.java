package com.bogos.delivery.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderFoodKey implements Serializable {

	
	@Column(name = "order_id")
	Integer orderId;

	@Column(name = "food_id")
	Integer foodId;
	
	 @Override
	    public int hashCode() {
	        return Objects.hash(getOrderId(), getFoodId());
	    }
	
	 @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (!(o instanceof OrderFoodKey)) return false;
	        OrderFoodKey that = (OrderFoodKey) o;
	        return Objects.equals(getOrderId(), that.getOrderId()) &&
	                Objects.equals(getFoodId(), that.getFoodId());
	    }
}
