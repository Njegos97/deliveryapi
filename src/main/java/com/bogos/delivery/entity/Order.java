package com.bogos.delivery.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private LocalDate dateOfOrder;

	private double priceOfOrder;

	private String sentTo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "users_id")
	private User user;

	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
	@JsonManagedReference
	private Set<OrderFood> orderFood = new HashSet<>();

	public void addFood(Food food) {
		OrderFood orderFoods = new OrderFood(this, food);
		orderFood.add(orderFoods);
		food.getOrderFood().add(orderFoods);
	}

	public void removeFood(Food food) {
		for (Iterator<OrderFood> iterator = orderFood.iterator(); iterator.hasNext();) {
			OrderFood orderFood = iterator.next();

			if (orderFood.getOrder().equals(this) && orderFood.getFood().equals(food)) {
				iterator.remove();
				orderFood.getFood().getOrderFood().remove(orderFood);
				orderFood.setOrder(null);
				orderFood.setFood(null);
			}
		}
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		return Objects.equals(id, other.getId());
	}
}
