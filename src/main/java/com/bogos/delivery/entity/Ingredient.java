package com.bogos.delivery.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bogos.delivery.enumerator.Category;
import com.bogos.delivery.enumerator.Type;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ingredient {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotBlank
	@Size(max = 50)
	@Size(min = 2)
	private String Name;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private Type type;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private Category category;

}
