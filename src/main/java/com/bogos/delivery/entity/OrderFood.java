package com.bogos.delivery.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class OrderFood {

	@EmbeddedId
	@JsonIgnoreProperties("id")
	OrderFoodKey id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @MapsId("order_id")
    @JoinColumn(name = "order_id")
	@JsonBackReference
    Order order;
	
	@ManyToOne()
    @MapsId("food_id")
    @JoinColumn(name = "food_id")
    Food food;
	
	private int foodQuantity;
	
	public OrderFood(Order order, Food food) {
        this.order = order;
        this.food = food;
        this.id = new OrderFoodKey(order.getId(), food.getId());
    }
}
