package com.bogos.delivery.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.bogos.delivery.entity.Cart;
import com.bogos.delivery.entity.Food;
import com.bogos.delivery.entity.Order;
import com.bogos.delivery.entity.OrderFood;
import com.bogos.delivery.entity.User;
import com.bogos.delivery.enumerator.Role;
import com.bogos.delivery.enumerator.Type;
import com.bogos.delivery.repository.OrderRepository;
import com.bogos.delivery.repository.UserRepository;

class OrderServiceImplTest {

	OrderRepository orderRepositoryMock = Mockito.mock(OrderRepository.class);
	UserRepository userRepositoryMock = Mockito.mock(UserRepository.class);
	OrderServiceImpl orderServiceDummy = new OrderServiceImpl(orderRepositoryMock, userRepositoryMock);

	@MockBean
	private SecurityContext securityContext;

	@MockBean
	private Authentication authentication;

	private Order order1;
	private Cart orderFoodDto1;
	private Cart orderFoodDto2;
	private Food food1;
	private Food food2;
	private User user1;
	private OrderFood orderFood1;

	private Set<Cart> orderFoodDtoList = new HashSet<>();
	private Set<OrderFood> orderFood = new HashSet<>();
	private List<Order> orders;

	@BeforeEach
	void setUp() throws Exception {

		user1 = new User();
		user1.setId(1);
		user1.setActive(true);
		user1.setAddress("Libery Street");
		user1.setEmail("JohnSmith@gmail.com");
		user1.setFirstName("John");
		user1.setLastName("Smith");
		user1.setRole(Role.USER);

		order1 = new Order();
		order1.setId(1);
		order1.setDateOfOrder(LocalDate.now());
		order1.setPriceOfOrder(22);
		order1.setSentTo("Libery Street");
		order1.setUser(user1);
		order1.setOrderFood(orderFood);

		food1 = new Food();
		food1.setId(1);
		food1.setName("Pepperoni Pizza");
		food1.setPrice(7.50);
		food1.setOutOfStock(false);
		food1.setType(Type.PIZZA);

		food2 = new Food();
		food2.setId(2);
		food2.setName("Big Mac");
		food2.setPrice(6);
		food2.setOutOfStock(false);
		food2.setType(Type.BURGER);

		orderFood1 = new OrderFood();
		orderFood1.setFood(food1);
		orderFood1.setOrder(order1);
		orderFood1.setFoodQuantity(2);

		orderFoodDto1 = new Cart();
		orderFoodDto1.setFood(food1);
		orderFoodDto1.setFoodQuantity(2);

		orderFoodDto2 = new Cart();
		orderFoodDto2.setFood(food2);
		orderFoodDto2.setFoodQuantity(1);

		orderFoodDtoList.add(orderFoodDto1);
		orderFoodDtoList.add(orderFoodDto2);

		orders = Arrays.asList(order1);

		orderFood.add(orderFood1);
	}

	@Test
	public void shouldSaveOrder() throws Exception {

		Authentication authentication = mock(Authentication.class);
		SecurityContext securityContext = mock(SecurityContext.class);
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		Mockito.when(orderRepositoryMock.save(order1)).thenReturn(order1);
		double orderPrice = 22;
		String sentTo = "Liberty Street";
		orderServiceDummy.saveOrder(orderFoodDtoList, orderPrice, sentTo);

		Order savedOrder = order1.getOrderFood().iterator().next().getOrder();

		assertThat(savedOrder.getPriceOfOrder()).isEqualTo(order1.getPriceOfOrder());
		assertThat(savedOrder.getUser()).isEqualTo(order1.getUser());

	}

	@Test
	public void shouldReturnListOfAllOrders() throws Exception {
		Mockito.when(orderRepositoryMock.findAll()).thenReturn(orders);

		List<Order> allOrders = orderServiceDummy.findAll();

		assertThat(allOrders.get(0).getId()).isSameAs(order1.getId());
	}

	@Test
	public void shouldReturnOrderByGivenId() throws Exception {
		Mockito.when(orderRepositoryMock.findById(1)).thenReturn(Optional.of(order1));
		Optional<Order> orderWithGivenId = orderServiceDummy.findOrder(1);

		assertThat(orderWithGivenId.get().getId()).isSameAs(order1.getId());

	}

}
