package com.bogos.delivery.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;

import com.bogos.delivery.entity.Food;
import com.bogos.delivery.enumerator.Type;
import com.bogos.delivery.repository.FoodRepository;

class FoodServiceImplTest {
	
	FoodRepository foodRepositoryMock = Mockito.mock(FoodRepository.class);
	FoodServiceImpl foodServiceDummy = new FoodServiceImpl(foodRepositoryMock);

	@Value("${CLOUDINARY_URL}")
	String cloudinary_url;
	
	private Food food1;
	private Food food2;
	private Food food3;
	private List<Food> foodList;
	
	@BeforeEach
	void setUp() throws Exception {

		food1 = new Food();
		food1.setId(1);
		food1.setName("Papparoni Pizza");
		food1.setPrice(7.50);
		food1.setOutOfStock(false);
		food1.setType(Type.PIZZA);

		food2 = new Food();
		food2.setId(2);
		food2.setName("Big Mac");
		food2.setPrice(6);
		food2.setOutOfStock(false);
		food2.setType(Type.BURGER);

		food3 = new Food();
		food3.setId(3);
		food3.setName("Cheesburger");
		food3.setPrice(4.50);
		food3.setOutOfStock(true);
		food3.setType(Type.BURGER);

		foodList = Arrays.asList(food1, food2, food3);

	}

//	@Test
//	public void shouldSaveSameFoodAsGiven() throws Exception {
//	
//		Mockito.when(foodRepositoryMock.save(food1)).thenReturn(food1);
//		Mockito.when(foodServiceDummy.uploadFoodImage(food1)).thenReturn("Succesfull");
//		Food savedFood = foodServiceDummy.save(food1);
//		
//		assertThat(savedFood.getName()).isSameAs(food1.getName());
//	}
	
//	@Test
//	public void shouldUpdateGivenFood() throws Exception {
//		Food updatedFood = new Food();
//		updatedFood.setId(1);
//		updatedFood.setName("Hamburger");
//		Mockito.when(foodRepositoryMock.save(food1)).thenReturn(updatedFood);
//		
//		updatedFood = foodServiceDummy.editFood(food1);
//		
//		assertThat(updatedFood.getId()).isEqualTo(food1.getId());
//		assertThat(updatedFood.getName()).isNotEqualTo(food1.getName());
//	}
	
	@Test
	public void shouldReturnListOfAllFood() throws Exception {
		Mockito.when(foodRepositoryMock.findAll()).thenReturn(foodList);
		
		List<Food> allFood = foodServiceDummy.findAll();
		
		assertThat(allFood.get(0).getName()).isSameAs(food1.getName());
		assertThat(allFood.get(1).getName()).isSameAs(food2.getName());
	}
	
	@Test
	public void shouldReturnFoodByGivenId() throws Exception {
		int id = 1;
		Food food = new Food();
		food.setId(id);
		food.setName("pizza");
		food.setPrice(5);
		food.setType(Type.PIZZA);
		
		Mockito.when(foodRepositoryMock.findById(id)).thenReturn(Optional.of(food));
		Food foodWithGivenId = foodServiceDummy.findById(id);
		
		assertThat(foodWithGivenId).isSameAs(food);
		
	}
	
	@Test
	public void shouldVerifyThatDeleteFoodIsCalled() throws Exception {
		int foodId = 3;
		
		foodServiceDummy.deleteById(foodId);
		
		verify(foodRepositoryMock, times(1)).deleteById(foodId);
	}
	
	@Test
	public void shouldReturnFoodByType() throws Exception {
		Type type = Type.BURGER;
		Mockito.when(foodRepositoryMock.findByType(type))
				.thenReturn(Arrays.asList(food2, food3));

		List<Food> result = foodServiceDummy.getFoodByType(type);

		verify(foodRepositoryMock, times(1)).findByType(type);
		assertThat(result.size()).isEqualTo(2);
		assertThat(result.get(0).getType()).isEqualTo(type);
		assertThat(result.get(1).getType()).isEqualTo(type);
		assertThat(result.get(0).getName()).isEqualTo("Big Mac");
		assertThat(result.get(1).getName()).isEqualTo("Cheesburger");
	}

}
