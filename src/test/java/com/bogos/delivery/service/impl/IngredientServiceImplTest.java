package com.bogos.delivery.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.bogos.delivery.entity.Ingredient;
import com.bogos.delivery.enumerator.Category;
import com.bogos.delivery.enumerator.Type;
import com.bogos.delivery.repository.IngredientRepository;

public class IngredientServiceImplTest {

	IngredientRepository ingredientRepositoryMock = Mockito.mock(IngredientRepository.class);
	IngredientServiceImpl ingredientServiceDummy = new IngredientServiceImpl(ingredientRepositoryMock);

	private Ingredient ingredient1;
	private Ingredient ingredient2;
	private Ingredient ingredient3;
	private List<Ingredient> ingredientList;

	@BeforeEach
	void setUp() throws Exception {

		ingredient1 = new Ingredient();
		ingredient1.setId(1);
		ingredient1.setName("Pepperoni");
		ingredient1.setCategory(Category.MEAT);
		ingredient1.setType(Type.PIZZA);

		ingredient2 = new Ingredient();
		ingredient2.setId(2);
		ingredient2.setName("Mushrooms");
		ingredient2.setCategory(Category.MEAT);
		ingredient2.setType(Type.PIZZA);

		ingredient3 = new Ingredient();
		ingredient3.setId(3);
		ingredient3.setName("Green Olives");
		ingredient3.setCategory(Category.SALAD);
		ingredient3.setType(Type.PIZZA);

		ingredientList = Arrays.asList(ingredient1, ingredient2, ingredient3);

	}

	@Test
	public void shouldSaveSameIngredientAsGiven() throws Exception {
		Mockito.when(ingredientRepositoryMock.save(ingredient1)).thenReturn(ingredient1);

		Ingredient savedIngredient = ingredientServiceDummy.save(ingredient1);

		assertThat(savedIngredient.getName()).isSameAs(ingredient1.getName());
	}

	@Test
	public void shouldReturnIngredientsByGivenType() throws Exception {
		Type type = Type.PIZZA;

		Mockito.when(ingredientRepositoryMock.findByType(type)).thenReturn(ingredientList);
		List<Ingredient> ingredientsWithSameType = ingredientServiceDummy.findByType(type);

		assertThat(ingredientsWithSameType).isSameAs(ingredientList);

	}

	@Test
	public void shouldReturnListOfAllIngredients() throws Exception {
		Mockito.when(ingredientRepositoryMock.findAll()).thenReturn(ingredientList);

		List<Ingredient> allIngredients = ingredientServiceDummy.findAll();

		assertThat(allIngredients.get(0).getName()).isSameAs(ingredient1.getName());
		assertThat(allIngredients.get(1).getName()).isSameAs(ingredient2.getName());
	}

	@Test
	public void shouldReturnIngredientByGivenId() throws Exception {
		Mockito.when(ingredientRepositoryMock.findById(1)).thenReturn(Optional.of(ingredient1));
		Optional<Ingredient> ingredientWithGivenId = ingredientServiceDummy.findById(1);

		assertThat(ingredientWithGivenId.get().getName()).isSameAs(ingredient1.getName());
		assertThat(ingredientWithGivenId.get().getCategory()).isSameAs(ingredient1.getCategory());
		assertThat(ingredientWithGivenId.get().getType()).isSameAs(ingredient1.getType());
	}

	@Test
	public void shouldUpdateGivenFood() throws Exception {
		Ingredient updatedIngredient = new Ingredient();
		updatedIngredient.setId(1);
		updatedIngredient.setName("Hamburger");
		Mockito.when(ingredientRepositoryMock.save(ingredient1)).thenReturn(updatedIngredient);

		updatedIngredient = ingredientServiceDummy.editIngredient(ingredient1);

		assertThat(updatedIngredient.getId()).isEqualTo(ingredient1.getId());
		assertThat(updatedIngredient.getName()).isNotEqualTo(ingredient1.getName());
	}

	@Test
	public void shouldVerifyThatDeleteIngredientIsCalled() throws Exception {
		int ingredientId = 3;

		ingredientServiceDummy.deleteById(ingredientId);

		verify(ingredientRepositoryMock, times(1)).deleteById(ingredientId);
	}

	@Test
	public void shouldReturnIngredientsByPageAndFilter() throws Exception {
		int mockPageNumber = 0;
		int mockPageSize = 5;
		Pageable pageable = PageRequest.of(mockPageNumber, mockPageSize);

		Page<Ingredient> returnPage = new PageImpl<Ingredient>(ingredientList, pageable, ingredientList.size());
		Mockito.when(ingredientRepositoryMock.findAll(pageable)).thenReturn(returnPage);
		Page<Ingredient> pageOfIngredients = ingredientRepositoryMock.findAll(pageable);

		assertThat(pageOfIngredients).isEqualTo(returnPage);
	}
	
	@Test
	public void shouldReturnIngredientByTypeAndCategory() throws Exception {
		int mockPageNumber = 0;
		int mockPageSize = 15;
		Type type = Type.PIZZA;
		Category category = Category.MEAT;
		Pageable pageable = PageRequest.of(mockPageNumber, mockPageSize);
		List<Ingredient> ingredients = Arrays.asList(ingredient1, ingredient2);
		Page<Ingredient> ingredientByTypeAndCategory = new PageImpl<Ingredient>(ingredients, pageable, ingredients.size());
		Mockito.when(ingredientRepositoryMock.findByCategoryAndType(pageable, category, type))
				.thenReturn(ingredientByTypeAndCategory);

		Page<Ingredient> result = ingredientServiceDummy.getIngredientByCategoryAndTypeAndPage(category, type, mockPageNumber);

		verify(ingredientRepositoryMock, times(1)).findByCategoryAndType(pageable, category, type);
		assertThat(result.getContent().get(0).getCategory()).isEqualTo(category);
		assertThat(result.getContent().get(1).getCategory()).isEqualTo(category);
		assertThat(result.getContent().get(0).getName()).isEqualTo("Pepperoni");
		assertThat(result.getContent().get(1).getName()).isEqualTo("Mushrooms");
	}
}
