package com.bogos.delivery.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import com.bogos.delivery.entity.Cart;
import com.bogos.delivery.entity.Food;
import com.bogos.delivery.entity.Order;
import com.bogos.delivery.entity.OrderFood;
import com.bogos.delivery.entity.User;
import com.bogos.delivery.enumerator.Role;
import com.bogos.delivery.enumerator.Type;
import com.bogos.delivery.service.OrderService;
import com.bogos.delivery.service.UserService;
import com.bogos.delivery.service.impl.AuthService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(OrderController.class)
@AutoConfigureMockMvc(addFilters = false)
class OrderControllerTest {

	@Autowired
	private MockMvc mockMvc;

	private final String API_URL = "/api";

	@MockBean
	private OrderService orderService;

	@MockBean
	AuthService authService;

	@MockBean
	UserService userService;

	private Order order1;
	private Order order2;
	private Cart orderFoodDto1;
	private Cart orderFoodDto2;
	private Food food1;
	private Food food2;
	private User user1;
	private OrderFood orderFood1;

	private Set<Cart> orderFoodDtoList = new HashSet<>();
	private Set<OrderFood> orderFood = new HashSet<>();
	private List<Order> orders;

	@BeforeEach
	void setUp() throws Exception {

		user1 = new User();
		user1.setId(1);
		user1.setActive(true);
		user1.setAddress("Libery Street");
		user1.setEmail("JohnSmith@gmail.com");
		user1.setFirstName("John");
		user1.setLastName("Smith");
		user1.setRole(Role.USER);

		order1 = new Order();
		order1.setId(1);
		order1.setDateOfOrder(LocalDate.now());
		order1.setPriceOfOrder(22);
		order1.setSentTo("Libery Street");
		order1.setUser(user1);
		order1.setOrderFood(orderFood);

		order2 = new Order();
		order2.setId(2);
		order2.setDateOfOrder(LocalDate.now());
		order2.setPriceOfOrder(15);

		food1 = new Food();
		food1.setId(1);
		food1.setName("Pepperoni Pizza");
		food1.setPrice(7.50);
		food1.setOutOfStock(false);
		food1.setType(Type.PIZZA);

		food2 = new Food();
		food2.setId(2);
		food2.setName("Big Mac");
		food2.setPrice(6);
		food2.setOutOfStock(false);
		food2.setType(Type.BURGER);

		orderFood1 = new OrderFood();
		orderFood1.setFood(food1);
		orderFood1.setOrder(order1);
		orderFood1.setFoodQuantity(2);

		orderFoodDto1 = new Cart();
		orderFoodDto1.setFood(food1);
		orderFoodDto1.setFoodQuantity(2);

		orderFoodDto2 = new Cart();
		orderFoodDto2.setFood(food2);
		orderFoodDto2.setFoodQuantity(1);

		orderFoodDtoList.add(orderFoodDto1);
		orderFoodDtoList.add(orderFoodDto2);

		orders = Arrays.asList(order1, order2);

		orderFood.add(orderFood1);
	}

	@Test
	void shoudReturnStatusCreatedWhenOrderIsSaved() throws Exception {
		double orderPrice = 22;
		String sentTo = "Libery Street";
		Mockito.when(orderService.saveOrder(orderFoodDtoList, orderPrice, sentTo)).thenReturn(order1);
		ObjectMapper mapper = new ObjectMapper();

		mockMvc.perform(post(API_URL + "/cart?orderPrice=" + orderPrice + "&sentTo=" + sentTo)
				.content(mapper.writeValueAsString(orderFoodDtoList)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated()).andExpect(jsonPath("$[0].food.name", Matchers.is("Big Mac")))
				.andExpect(jsonPath("$[1].food.name", Matchers.is("Pepperoni Pizza")));
				
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void shoudReturnListOfOrdersAndStatusOk() throws Exception {
		Mockito.when(orderService.getOrderHistory()).thenReturn(orders);
		ObjectMapper mapper = new ObjectMapper();

		mockMvc.perform(get(API_URL + "/cart").content(mapper.writeValueAsString(orders))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id", Matchers.is(1))).andExpect(jsonPath("$[1].id", Matchers.is(2)));
	}

	@Test
	void shoudReturnListOfOrderContentByIdAndStatusOk() throws Exception {
		Mockito.when(orderService.findOrder(1)).thenReturn(Optional.of(order1));
		ObjectMapper mapper = new ObjectMapper();

		Set<OrderFood> orderedFood = order1.getOrderFood();

		mockMvc.perform(get(API_URL + "/cart/1").content(mapper.writeValueAsString(orderedFood))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].food.name", Matchers.is("Pepperoni Pizza")))
				.andExpect(jsonPath("$[0].foodQuantity", Matchers.is(2)));
	}

}
