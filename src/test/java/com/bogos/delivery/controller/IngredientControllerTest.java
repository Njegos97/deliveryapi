package com.bogos.delivery.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import com.bogos.delivery.entity.Ingredient;
import com.bogos.delivery.enumerator.Category;
import com.bogos.delivery.enumerator.Type;
import com.bogos.delivery.service.IngredientService;
import com.bogos.delivery.service.UserService;
import com.bogos.delivery.service.impl.AuthService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(IngredientController.class)
@AutoConfigureMockMvc(addFilters = false)
public class IngredientControllerTest {
	@Autowired
	private MockMvc mockMvc;

	private final String API_URL = "/api";

	@MockBean
	private IngredientService ingredientService;
	
	@MockBean
	AuthService authService;
	
	@MockBean
	UserService userService;

	private Ingredient ingredient1;
	private Ingredient ingredient2;
	private Ingredient ingredient3;
	private List<Ingredient> ingredientList;

	@BeforeEach
	void setUp() throws Exception {

		ingredient1 = new Ingredient();
		ingredient1.setId(1);
		ingredient1.setName("Pepperoni");
		ingredient1.setCategory(Category.MEAT);
		ingredient1.setType(Type.PIZZA);

		ingredient2 = new Ingredient();
		ingredient2.setId(2);
		ingredient2.setName("Mushrooms");
		ingredient2.setCategory(Category.MEAT);
		ingredient2.setType(Type.PIZZA);

		ingredient3 = new Ingredient();
		ingredient3.setId(3);
		ingredient3.setName("Green Olives");
		ingredient3.setCategory(Category.SALAD);
		ingredient3.setType(Type.PIZZA);

		ingredientList = Arrays.asList(ingredient1, ingredient2, ingredient3);
	}
	
	@Test
	@WithMockUser(roles = "ADMIN")
	void shoudReturnStatusCreatedWhenIngredientIsSaved() throws Exception {
		Mockito.when(ingredientService.save(any(Ingredient.class))).thenReturn(ingredient1);
		ObjectMapper mapper = new ObjectMapper();
		
		mockMvc.perform(
				post(API_URL + "/ingredient").content(mapper.writeValueAsString(ingredient1))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.name", Matchers.is("Pepperoni")));
	}
	
	@Test
	@WithMockUser(roles = "ADMIN")
	void shoudReturnIngredientByGivenTypeAndStatusOk() throws Exception{
		Type type = Type.PIZZA;
		Mockito.when(ingredientService.findByType(type)).thenReturn(ingredientList);
		ObjectMapper mapper = new ObjectMapper();
		
		mockMvc.perform(
				get(API_URL + "/ingredient/type?type=" + type).content(mapper.writeValueAsString(ingredientList))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].name", Matchers.is("Pepperoni")))
				.andExpect(jsonPath("$[1].name", Matchers.is("Mushrooms")))
				.andExpect(jsonPath("$[2].name", Matchers.is("Green Olives")));
	}
	
	@Test
	@WithMockUser(roles = "ADMIN")
	void shoudReturnListOfAllIngredientsAndStatusOk() throws Exception{
		Mockito.when(ingredientService.findAll()).thenReturn(ingredientList);
		ObjectMapper mapper = new ObjectMapper();
		
		mockMvc.perform(
				get(API_URL + "/ingredient").content(mapper.writeValueAsString(ingredientList))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].name", Matchers.is("Pepperoni")))
				.andExpect(jsonPath("$[1].name", Matchers.is("Mushrooms")))
				.andExpect(jsonPath("$[2].name", Matchers.is("Green Olives")));
	}
	
	@Test
	@WithMockUser(roles = "ADMIN")
	void shoudReturnIngredientByGivenIdAndStatusOk() throws Exception{
		Mockito.when(ingredientService.findById(1)).thenReturn(Optional.of(ingredient1));
		ObjectMapper mapper = new ObjectMapper();
		
		mockMvc.perform(
				get(API_URL + "/ingredient/1").content(mapper.writeValueAsString(ingredient1))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", Matchers.is("Pepperoni")))
				.andExpect(jsonPath("$.category", Matchers.is("MEAT")))
				.andExpect(jsonPath("$.type", Matchers.is("PIZZA")));
	}
	
	@Test
	@WithMockUser(roles = "ADMIN")
	void shoudReturnStatusOkWhenIngredientIsEdited() throws Exception{
		Ingredient updatedIngredient = new Ingredient();
		updatedIngredient.setId(1);
		updatedIngredient.setName("Tomato");
		updatedIngredient.setType(Type.BURGER);
		updatedIngredient.setCategory(Category.SALAD);
		Mockito.when(ingredientService.editIngredient(ingredient1)).thenReturn(updatedIngredient);
		ObjectMapper mapper = new ObjectMapper();
		
		mockMvc.perform(
				put(API_URL + "/ingredient/" + ingredient1.getId()).content(mapper.writeValueAsString(ingredient1))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", Matchers.is("Tomato")))
				.andExpect(jsonPath("$.category", Matchers.is("SALAD")))
				.andExpect(jsonPath("$.type", Matchers.is("BURGER")));
	}
	
	@Test
	@WithMockUser(roles = "ADMIN")
	void shoudReturnStatusOkWhenDeletedById() throws Exception{
		int ingredientId = 3;
		
		mockMvc.perform(delete(API_URL + "/ingredient/" + ingredientId)).andExpect(status().isOk());		
	}
	
	@Test
	@WithMockUser(roles = "ADMIN")
	public void shouldReturnIngredientsByPage() throws Exception {
		int mockPageNumber = 0;
		int mockPageSize = 15;
		Pageable pageable = PageRequest.of(mockPageNumber, mockPageSize);
		
		Page<Ingredient> returnPage = new PageImpl<Ingredient>(ingredientList, pageable, ingredientList.size());
		Mockito.when(ingredientService.findIngredientsByPage(mockPageNumber)).thenReturn(returnPage);
		
		this.mockMvc
		.perform(get(API_URL + "/ingredient/page?pageNumber=0"))
		.andExpect(status().isOk()).andExpect(jsonPath("$.content[0].name", Matchers.is("Pepperoni")))
		.andExpect(status().isOk()).andExpect(jsonPath("$.content[1].name", Matchers.is("Mushrooms")))
		.andExpect(status().isOk()).andExpect(jsonPath("$.content[2].name", Matchers.is("Green Olives")))
		.andExpect(status().isOk()).andExpect(jsonPath("$.totalElements", Matchers.is(ingredientList.size())))
		.andExpect(status().isOk()).andExpect(jsonPath("$.number", Matchers.is(mockPageNumber)));

	}
	
	@Test
	@WithMockUser(roles = "ADMIN")
	void shoudReturnIngredientsByTypeAndStatusOK() throws Exception {
		Type type = Type.PIZZA;
		Category category = Category.MEAT;
		int mockPageNumber = 0;
		int mockPageSize = 15;
		Pageable pageable = PageRequest.of(mockPageNumber, mockPageSize);
		List<Ingredient> ingredients = Arrays.asList(ingredient1, ingredient2);
		Page<Ingredient> ingredientByTypeAndCategory = new PageImpl<Ingredient>(ingredients, pageable, ingredients.size());

		Mockito.when(ingredientService.getIngredientByCategoryAndTypeAndPage(category, type, 0)).thenReturn(ingredientByTypeAndCategory);
		
		this.mockMvc.perform(get(API_URL + "/ingredient/category?pageNumber=0&category=MEAT&type=PIZZA")).andExpect(status().isOk())
				.andExpect(jsonPath("$.content[0].name", Matchers.is("Pepperoni")))
				.andExpect(jsonPath("$.content[0].type", Matchers.is("PIZZA")))
				.andExpect(jsonPath("$.content[1].name", Matchers.is("Mushrooms")))
				.andExpect(jsonPath("$.content[1].type", Matchers.is("PIZZA")));
	}

}
