package com.bogos.delivery.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import com.bogos.delivery.entity.Food;
import com.bogos.delivery.enumerator.Type;
import com.bogos.delivery.service.FoodService;
import com.bogos.delivery.service.UserService;
import com.bogos.delivery.service.impl.AuthService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(FoodController.class)
@AutoConfigureMockMvc(addFilters = false)
class FoodControllerTest {

	@Autowired
	private MockMvc mockMvc;

	private final String API_URL = "/api";

	@MockBean
	private FoodService foodService;

	@MockBean
	AuthService authService;

	@MockBean
	UserService userService;

	private Food food1;
	private Food food2;
	private Food food3;
	private List<Food> foodList;

	@BeforeEach
	void setUp() throws Exception {

		food1 = new Food();
		food1.setId(1);
		food1.setName("Papparoni Pizza");
		food1.setPrice(7.50);
		food1.setOutOfStock(false);
		food1.setType(Type.PIZZA);

		food2 = new Food();
		food2.setId(2);
		food2.setName("Big Mac");
		food2.setPrice(6);
		food2.setOutOfStock(false);
		food2.setType(Type.BURGER);

		food3 = new Food();
		food3.setId(3);
		food3.setName("Cheesburger");
		food3.setPrice(4.50);
		food3.setOutOfStock(true);
		food3.setType(Type.BURGER);

		foodList = Arrays.asList(food1, food2, food3);

	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void shoudReturnStatusCreatedWhenFoodIsSaved() throws Exception {
		Mockito.when(foodService.save(any(Food.class))).thenReturn(food1);
		ObjectMapper mapper = new ObjectMapper();

		mockMvc.perform(post(API_URL + "/food").content(mapper.writeValueAsString(food1))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
				.andExpect(jsonPath("$.name", Matchers.is("Papparoni Pizza")));
	}

	@Test
	void shoudReturnListOfAllFoodAndStatusOk() throws Exception {
		Mockito.when(foodService.findAll()).thenReturn(foodList);
		ObjectMapper mapper = new ObjectMapper();

		mockMvc.perform(get(API_URL + "/food").content(mapper.writeValueAsString(foodList))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].name", Matchers.is("Papparoni Pizza")))
				.andExpect(jsonPath("$[1].name", Matchers.is("Big Mac")))
				.andExpect(jsonPath("$[2].name", Matchers.is("Cheesburger")));
	}

	@Test
	void shoudReturnFoodByGivenIdAndStatusOk() throws Exception {
		Mockito.when(foodService.findById(1)).thenReturn(food1);
		ObjectMapper mapper = new ObjectMapper();

		mockMvc.perform(get(API_URL + "/food/1").content(mapper.writeValueAsString(food1))
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.name", Matchers.is("Papparoni Pizza")))
				.andExpect(jsonPath("$.price", Matchers.is(7.5))).andExpect(jsonPath("$.type", Matchers.is("PIZZA")));
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void shoudReturnStatusOkWhenFoodIsEdited() throws Exception {
		Food updatedFood = new Food();
		updatedFood.setId(1);
		updatedFood.setName("Hamburger");
		updatedFood.setType(Type.BURGER);
		updatedFood.setPrice(5.5);
		Mockito.when(foodService.editFood(food1)).thenReturn(updatedFood);
		Mockito.when(foodService.findById(1)).thenReturn(food1);
		ObjectMapper mapper = new ObjectMapper();

		mockMvc.perform(put(API_URL + "/food/" + food1.getId() + "?isImageChanged=false")
				.content(mapper.writeValueAsString(food1)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.name", Matchers.is("Hamburger")))
				.andExpect(jsonPath("$.price", Matchers.is(5.5))).andExpect(jsonPath("$.type", Matchers.is("BURGER")));
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void shoudReturnStatusOkWhenDeletedById() throws Exception {
		int foodId = 3;

		mockMvc.perform(delete(API_URL + "/food/" + foodId)).andExpect(status().isOk());
	}

	@Test
	void shoudReturnFoodByTypeAndStatusOK() throws Exception {
		Type type = Type.BURGER;
		List<Food> foodByType = Arrays.asList(food2, food3);

		Mockito.when(foodService.getFoodByType(type)).thenReturn(foodByType);

		this.mockMvc.perform(get(API_URL + "/food/type?type=BURGER")).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].name", Matchers.is("Big Mac")))
				.andExpect(jsonPath("$[0].type", Matchers.is("BURGER")))
				.andExpect(jsonPath("$[1].name", Matchers.is("Cheesburger")))
				.andExpect(jsonPath("$[1].type", Matchers.is("BURGER")));
	}

}
