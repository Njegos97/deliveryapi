# Delivery Food API
REST API for ordering food online created with Java, Spring, PostqreSQL.

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Prerequisites](#prerequisites)
* [Setup PostgreSQL](#setup-postgresql)
* [Setup Api](#setup-api)
* [Features](#features)
* [Status](#status)
* [Contact](#contact)

## General info
The API is designed around REST. Accepts JSON-encoded request bodies, returns JSON-encoded responses, and uses standard HTTP response codes, authentication, and verbs.
	
## Technologies
Project is created with:
* Java version: 11
* Spring version: 2.3.1
* PostgreSQL version: 12.3
* Swagger version: 2.9.2
* Google Api version: 1.30.9
* Cloudinary version: 1.26.0
* Flyway
* Lombok

## Prerequisites
You can skip this step if you already have the prerequisites installed

* JDK - download and install: https://jdk.java.net/15/
* Maven - download and install: https://maven.apache.org/download.cgi
* PostgreSQL  - download and install(Set username to: "postgres", set password to: "root"): https://www.postgresql.org/download/
* Git - download and install: https://git-scm.com/downloads
* Lombok- for install follow this link: https://www.baeldung.com/lombok-ide

## Setup PostgreSQL

To create datebase with SQL Shell, follow this steps:

* Open the SQL Shell
* Press enter four times
* For password enter: root
When you get access enter following script:
* CREATE DATABASE deliveri;
* ![psql-shell](/uploads/0776349dc12cbd42e47db7b0963763c5/psql-shell.png)

To create datebase with pgAdmin, follow this steps:
* Open pgAdmin
* ![postgresql-create-database](/uploads/70553f946841aaa3036e141e19d136a9/postgresql-create-database.png)
* For datebase name enter: deliveri

## Setup Api
To run Delivery API with cmd, open cmd and enter following scripts: 

* git clone https://gitlab.com/Njegos97/deliveryapi.git - "Clone the repository"
* cd deliveryapi - "Navigate to project root"
* mvn spring-boot:run - "Start the application"

To run Delivery API with eclipse, follow next steps:

* Download eclipse from: https://www.eclipse.org/downloads/
* Install Eclipse IDE for Enterprise Java Develepores
* git clone https://gitlab.com/Njegos97/deliveryapi.git - "Clone the repository"
* Open eclipse
* Import project in your eclipse workspace
* Go to eclipse marketplace (Help -> Eclipse Marketplace) and enter: "Spring sts"
* Install Spring Tool Suite 4
* Restart Eclipse
* if you have troubles with lombok, follow this link: https://stackoverflow.com/questions/11803948/lombok-is-not-generating-getter-and-setter
* If you have more troubles try right-click on project -> Maven -> Update Project
* Run project as spring boot app

* go to http://localhost:8080/swagger-ui.html#/ to see the available endpoints

## Features

* Add Food
* Edit Food
* Find Food by ID, 
* Find Food by Type
* Delete food
* Add ingredient
* Edit Ingredient
* Find Ingredient by ID
* Find Ingredients by Type, Category
* Delete Ingredient 
* Add Order
* Get User Order
* Get All Orders
* Get Order By Id
* Get Users
* Get Logged In User
* Get User By Id
* Change User Role
* Add New User
* Authorization and Authentication
* Cloudinary Image Upload
* Swagger
 
## Status
Project is: in progress

## Contact
* Created by - Njegos Puljanovic
* Feel free to contact me at - njegoss997@gmail.com
* LinkedIn: https://www.linkedin.com/in/njegos-puljanovic-9a01a41ba/
